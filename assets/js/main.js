

 
 $(document).ready(function($) {'use strict',
	//for owl carousel 
		$("#owl-demo").owlCarousel({
		 
		autoPlay: 3000, //Set AutoPlay to 3 seconds
		items :4,
		itemsDesktop : [1199,3],
		itemsDesktopSmall : [979,3]
		
    });
	
	
	//for revolution slider
		 
		var tpj=jQuery;               // MAKE JQUERY PLUGIN CONFLICTFREE  
	   tpj(document).ready(function() {
				 if (tpj.fn.cssOriginal!=undefined)   // CHECK IF fn.css already extended
                   tpj.fn.css = tpj.fn.cssOriginal;
				   
				tpj('.fullwidthbanner').revolution(
					{    
						delay:9000,                                                
						startheight:405,                            
						startwidth:890,                
						touchenabled:"on",                      
						onHoverStop:"on",                        
						fullWidth:"on" ,   
													
					});	
				});
					
					
	//for header effect
	$(window).bind('scroll', function() {
        var navHeight = $(window).height() -100;
        if ($(window).scrollTop() > 50) {
            $('.navbar-default').addClass('on');
        } else {
            $('.navbar-default').removeClass('on');
        }
     });
	 

	 
	 //for animated
	 new WOW().init();
	 
	 
	    //for fancybox
	 $(".fancybox").fancybox({
		padding: 0,

		openEffect : 'elastic',
		openSpeed  : 650,

		closeEffect : 'elastic',
		closeSpeed  : 550,
		closeClick : true,
	});


	//for fancybox-youtube
	 $(".fancybox-youtube").fancybox({
		'transitionIn'  : 'elastic',
		'transitionOut' : 'fade',
		'width'         : 680,
		'height'        : 495,
		'type'          :  'swf',
	});
	 if(location.hash){
	   $(location.hash).click();
	 }
	 
	 
	 //for preloader
	 (function () {
	    $(window).load(function() {
	        $('#preloader-item').fadeOut();
	        $('#preloader').delay(350).fadeOut('slow');
	    });
	}());
	
   });
   

	
		// Skill bar Function
		
	$(document).ready(function(){
	  "use strict";
	  $('.skillbar').appear(function() {
		$(this).find('.skillbar-bar').animate({
		  width:$(this).attr('data-percent')
		},3000);
	  });
	});
	 
    //for parallax effect	
       $('.portfolio-parallax').parallax({imageSrc:'image/p-bg2.jpg'}); 
	

	//for isotope hover effect
	$(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );

	
	//for scroll to top
	var btt= $('.bt-top');  
	    btt.on('click',function (){
		   $('html,body').animate({
		      scrollTop:0
		   },500);
		});
	
	
	//for # link
	$('#main-menu li a[href*=#]').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
		&& location.hostname == this.hostname) {
		  var $target = $(this.hash);
		  $target = $target.length && $target
		  || $('[name=' + this.hash.slice(1) +']');
		  if ($target.length) {
			var targetOffset = $target.offset().top;
			$('html,body')
			.animate({scrollTop: targetOffset}, 3000);
		   return false;
		  }
		}
	});
			  

	
	  // for isotope
  var $container = $('.isotope').isotope({
    itemSelector: '.element-item',
    layoutMode: 'fitRows',
    getSortData: {
      name: '.name',
      symbol: '.symbol',
      number: '.number parseInt',
      category: '[data-category]',
      weight: function( itemElem ) {
        var weight = $( itemElem ).find('.weight').text();
        return parseFloat( weight.replace( /[\(\)]/g, '') );
      }
    }
  });

  // filter functions
  var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
      var number = $(this).find('.number').text();
      return parseInt( number, 10 ) > 50;
    },
    // show if name ends with -ium
    ium: function() {
      var name = $(this).find('.name').text();
      return name.match( /ium$/ );
    }
  };

  // bind filter button click
  $('#filters').on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });
  });

  // bind sort button click
  $('#sorts').on( 'click', 'button', function() {
    var sortByValue = $(this).attr('data-sort-by');
    $container.isotope({ sortBy: sortByValue });
  });
  
  // change is-checked class on buttons
  $('.button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $( this ).addClass('is-checked');
    });
  });
  
 
